const siteLogo = document.querySelector("#site-logo");
const header = document.querySelector("header");
const content = document.querySelector("#content");

const siteNav = document.querySelector("#site-nav");
const navItemUl = document.querySelector("#site-nav ul");
const mobileNavMenuBtn = document.querySelector("#mobile-nav-menu-btn");
const mobileNavMenu = document.querySelector(".mobile-nav-menu");

const hamburgerMenuMobile = "<i class='fa fa-bars' aria-hidden='true'></i>";
const closeMenuMobile = "<i class='fa fa-times' aria-hidden='true'></i>";

function renderMenu(windowWidth) {
    if (windowWidth < 1199) {
        siteLogo.setAttribute("src", "images/jhpiego-mamagram-white.svg");
        siteNav.classList.add("mobile-nav-menu");
        navItemUl.classList.add("mobile-nav-closed");
    } else {
        siteLogo.setAttribute("src", "images/jhpiego-logo-white.svg");
        siteNav.classList.remove("mobile-nav-menu");
        navItemUl.style.display = "flex";
    }
}

window.onload = () => {
    renderMenu(window.innerWidth);
}

window.onresize = () => {
    renderMenu(window.innerWidth);
}

// I need to figure something out for the home page. This is for all other pages
window.onscroll = () => {
    if(window.pageYOffset >= 100) {
        header.classList.remove("header-full");
        header.classList.add("header-scroll");
    } else {
        header.classList.add("header-full");
        header.classList.remove("header-scroll");
    }
}

mobileNavMenuBtn.innerHTML = hamburgerMenuMobile;
mobileNavMenuBtn.addEventListener("click", () => {
    if (!navItemUl.classList.contains("mobile-nav-open")) {
        mobileNavMenuBtn.innerHTML = closeMenuMobile;
        navItemUl.classList.add("mobile-nav-open");
        navItemUl.classList.remove("mobile-nav-closed");
    } else {
        mobileNavMenuBtn.innerHTML = hamburgerMenuMobile;
        navItemUl.classList.add("mobile-nav-closed");
        navItemUl.classList.remove("mobile-nav-open");
    }
});